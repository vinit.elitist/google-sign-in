import React from 'react';
import Typography from '@mui/material/Typography';

const CustomTypography = ({ variant, className, children, href, underline, style }) => {
  return (
    <Typography variant={variant} className={className} style={style}>
      {href ? <a href={href} style={{ textDecoration: underline ? 'underline' : 'none' }}>{children}</a> : children}
    </Typography>
  );
};

export default CustomTypography;