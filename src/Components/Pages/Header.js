import React from 'react';
import Box from '@mui/material/Box';

import googleLogo from '../../assests/Images/Google-Logo-3.png';
import CustomTypography from '../Typography/CustomTypography';

const Header = () => {
  return (
    
    <Box className="hero">
      <img className="image" src={googleLogo} alt="Google Logo" />
      <CustomTypography className="heading">Sign in</CustomTypography>
      <CustomTypography variant='h4' className="sub-heading">Use your Google Account</CustomTypography>
    </Box>
  );
}

export default Header;
