// Footer.js

import React from 'react';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';

import '../../style/Common/Footer.scss';
import DropDown from '../DropDown/DropDown';
import CustomTypography from '../Typography/CustomTypography'; // Import the CustomTypography component

const Footer = () => {
  const languages = [
    { label: 'ગુજરાતી' },
    { label: 'Francais (Canada)' },
    { label: 'Francais (France)' },
    { label: 'Indonesia' },
    { label: 'norsk' },
    { label: 'Romana' },
    { label: 'मराठी' },
    { label: 'हिन्दी' },
    { label: 'नेपाली' },
    { label: 'മലയാളം' },
  ];

  return (
    <Box className="footer">
      <Box className="lang">
        <DropDown label={"English (US)"} options={languages} className="dropdown" />
      </Box>

      <Box className="services">

        <Link href="#" color="inherit" underline='none'>
          <CustomTypography variant='p'>Help</CustomTypography>
        </Link>

        <Link href="#" color="inherit" underline='none'>
          <CustomTypography variant='p'>Privacy</CustomTypography>
        </Link>

        <Link href="#" color="inherit" underline='none'>
          <CustomTypography variant='p'>Terms</CustomTypography>
        </Link>
      </Box>
    </Box>
  );
};

export default Footer;
