import React from 'react';
import { TextField } from '@mui/material';

const CustomTextField = ({ label, id, variant, textFieldStyle }) => {
  return (
    <TextField
      className="textField"
      id={id}
      label={label}
      variant={variant}
      style={textFieldStyle}
    />
  );
};

export default CustomTextField;