import { FormControl, InputLabel, Select, MenuItem } from '@mui/material';
import React from 'react';
import "../../style/Common/DropDown.scss"

const DropDown = ({ label, options, ...props }) => {
  return (
    <FormControl variant="standard" className='formcontrol standard'>
      <InputLabel className='label'>{label}</InputLabel>
      <Select {...props}>
        {options.map((option) => (
          <MenuItem key={option.label} value={option.label}>
            {option.label}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};
export default DropDown;