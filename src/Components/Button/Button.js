import React from 'react';
import { Button as MUIButton } from '@mui/material';

const Button = ({ variant, className, size, children }) => {
  return (
    <MUIButton variant={variant} className={className} size={size} style={{marginLeft:-10}}>
      {children}
    </MUIButton>
  );
}

export default Button;