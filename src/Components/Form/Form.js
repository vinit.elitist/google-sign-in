// Form.js

import React from 'react';
import Link from '@mui/material/Link';

import Box from '@mui/material/Box';

import {createTheme, ThemeProvider} from '@mui/material/styles'

import Button from '../Button/Button';
import CustomTextField from '../TextFeild/TextFieldComponent';
import CustomTypography from '../Typography/CustomTypography';  // Import the CustomTextField component
import "../../style/Common/Form.scss";


const theme = createTheme({
  typography: {
   
    
  },  
});
const Form = () => {
  return (

    <ThemeProvider theme={theme}>   
      <form className="form">

        <CustomTextField
          id="outlined-basic"
          label="Email or Phone"
          variant="outlined"
          textFieldStyle={{ width: 380,marginTop:15 }}
        />

        <Link href="#" color="inherit" underline='none' >
          <CustomTypography variant='inherit' className='forgotEmail'>
            Forgot email ?
          </CustomTypography>
        </Link>

        <CustomTypography className='subtext'>
          Not your computer? Use Guest mode to sign in privately.
        </CustomTypography>

        <Link href="#" color="inherit" underline='none' style={{ fontSize: 14 }} className='subtext2'>
          <CustomTypography variant='inherit' underline={false}>
            Learn more
          </CustomTypography>
        </Link>

        {/* Button Part */}
        <Box className='buttons'>
          <Button variant="text" className='createAccount'> Create Account</Button>
          
          <Box style={{ marginLeft: 155 }}>
            <Button variant="contained" className='medium' size="medium">Next</Button>
          </Box>
        </Box>
      </form>
    </ThemeProvider>

  );
};

export default Form;
