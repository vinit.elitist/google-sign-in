import React from 'react';

import Box from '@mui/material/Box';


import Header from '../Pages/Header';
import Form from '../Form/Form';
import Footer from '../Pages/Footer';
import '../../style/Common/Home.scss';

const Home = () => {
  return (
    <Box>
      <Box className="container">
        <Header />  
        <Form />
      </Box>
      <Footer />
    </Box>
  );
}

export default Home;
